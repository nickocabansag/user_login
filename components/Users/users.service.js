module.exports = function UserService(dbo) {

    const insertUser = (data) => {
        return new Promise((resolve, reject) => {
            var obj = { username: data.user, password: data.pass };
            return dbo.collection("users").insertOne(obj, function(err, res) {
                if (err) throw err;
                resolve(res);
            });   
        })
        
    }

    const loginUser = (data) => {
        return new Promise((resolve, reject) => {
            var obj = { username: data.user }
            return dbo.collection("users").find(obj).toArray(function(err, result) {
                if (err) throw err;
                // console.log(result);
                resolve(result);
            }); 
        })
    }
    
    return {
        'insertUser': insertUser,
        'loginUser': loginUser
    }
}