module.exports = function(dbo) {
    //Instantiate service and inject data
    const userService = require('./users.service.js')(dbo);
    //Instantiate controller and inject config and service
    const userController = require('./users.controller.js')(userService);
    //Instantiate router and inject controller
    const userRouter = require('./users.router.js').UserRouter(userController);
    
    return userRouter;
}