const express = require('express');
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json();

module.exports.UserRouter = (userController) => {
    const router = express.Router();
    router.use(jsonParser);

    // create a route for /api/customers
    router.post('/register', jsonParser, (req, res) => {
        userController.insertUser(req.body).then(function(result){
            let obj = { message: 'Registered Successfully!' }
            res.status(201).send(obj); // set 200 (OK) status
        })
    });

    router.post('/login', jsonParser, (req, res) => {
        userController.loginUser(req.body).then(function(result){
            let obj = { }
            if(req.body.pass == result[0].password) {
                obj = { message: 'Login Successfully!' }
                res.status(200).send(obj); // set 200 (OK) status
            }
            else
            {
                obj = { message: 'username/password is wrong!' }
                res.status(200).send(obj); // set 200 (OK) status
            }
        })
    });

    return router;
}