module.exports = function UserController(userService) {

    const insertUser = (data) => {
        return userService.insertUser(data);
    }

    const loginUser = (data) => {
        return userService.loginUser(data);
    }

    // Return methods available for the controller
    return { 
        'insertUser': insertUser,
        'loginUser': loginUser
    }
}